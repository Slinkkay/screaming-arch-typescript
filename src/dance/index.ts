import turnLights from "./turn-light";
import playSound from "./play-sound";

export default function () {
  turnLights("1", "blue");
  turnLights("2", "green");
  playSound("Bell");
}