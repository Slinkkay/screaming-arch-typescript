import dance from "@dance/index";
import { checkConfiguration } from "@configuration";
import configConsole from "./console-configuration";

configConsole();

checkConfiguration();

dance();
