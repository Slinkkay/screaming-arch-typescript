import cache, { FunctionConfigure } from "@configuration";
export type COLOR = "blue" | "green";
export type ID = "1" | "2";

export interface TurnLight {
  (id: ID, color: COLOR): void
}

const local: FunctionConfigure<TurnLight> = cache("turn-lights");

export const setup = (impl: TurnLight) => local.configure(impl);

export default (id: ID, color: COLOR) => local.fetch()(id, color);
