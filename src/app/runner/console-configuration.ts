import { setup as configureLight } from "@dance/turn-light";
import { setup as configureSound } from "@dance/play-sound";
import consoleLight from "@adapter/console-light";
import consoleSound from "@adapter/console-sound";

export default function() {
    configureSound(consoleSound);
    configureLight(consoleLight);
}
