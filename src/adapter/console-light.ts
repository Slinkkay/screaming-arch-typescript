import {ID, COLOR, TurnLight} from "@dance/turn-light";

const turnLight: TurnLight = (id: ID, color: COLOR) => {
    console.log(`id: ${id}, Color: ${color}`);
};

export default turnLight;
