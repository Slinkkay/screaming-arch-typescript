const path = require("path");
const fs = require("fs");

function srcPath(subdir) {
    return path.join(__dirname, "src", subdir);
}

function listConsole() {
    const result = fs.readdirSync("./src/app/console");

    const entryMap = {};

    result.forEach((item) => {
        if (item.endsWith(".ts")) {
            item = item.substr(0, item.length - 3);
        }
        entryMap[item] = `./src/app/console/${item}`;
    });
    entryMap["runner"] = "./src/runner.ts";
    return entryMap;
}

function runnerExists() {
    return fs.existsSync("./src/runner.ts");
}

const configLocation = "./tsconfig.json";

/**
 * Finds all paths configured in tsconfig.json and adds them as alias for webpack
 */
function findPaths() {
    const tsConfig = JSON.parse(fs.readFileSync(configLocation).toString());

    let alias = {};
    let pathRef = tsConfig.compilerOptions.paths;
    for (let property in pathRef) {
        if (pathRef.hasOwnProperty(property)) {
            // Get the name of the alias and it's reference Multiple references aren't supported
            let aliasName = property;
            let aliasReference = tsConfig.compilerOptions.paths[property][0];
            // If alias ends with a wildcard then webpack item needs it removed for both items
            if (property.endsWith("/*")) {
                aliasName = property.substr(0, property.length - 2);
                aliasReference = aliasReference.substr(
                    0,
                    aliasReference.length - 2,
                );
            }
            alias[aliasName] = srcPath(aliasReference);
        }
    }
    return alias;
}

function getTsConfig() {
    return JSON.parse(fs.readFileSync(configLocation).toString());
}

function addDeclarationToConfig(config) {
    config.compilerOptions.declaration = true;
    return config;
}

exports.findPaths = findPaths;
exports.runnerExists = runnerExists;
exports.listConsole = listConsole;
exports.addDeclarationToConfig = addDeclarationToConfig;
exports.getTsConfig = getTsConfig;
