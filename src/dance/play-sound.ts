import cache, { FunctionConfigure } from "@configuration";
export type Sound = "Horn" | "Bell" | { value: "string"};

export interface PlaySound {
  (sound: Sound): void
}

const local: FunctionConfigure<PlaySound> = cache("play-sound");

export const setup = (impl: PlaySound) => local.configure(impl);

export default (sound: Sound) => local.fetch()(sound);
