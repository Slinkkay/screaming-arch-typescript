const source = require("./webpack.config");

const testingSource = {
    ...source,
    module: {
        ...source.module,
        rules: [
            {
                test: /\.(js|ts)/,
                include: /\.(js|ts)/,
                exclude: /\.(spec.ts)/,
                loader: "istanbul-instrumenter-loader",
            },
            ...source.module.rules
        ]
    }
};

module.exports = testingSource;
