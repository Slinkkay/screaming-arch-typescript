export class FunctionConfigure<T> {
  configuredFunction: T;
  listener: () => void;
  name: string;
  constructor(name: string) {
    this.name = name;
  }

  configure(impl: T) {
    this.configuredFunction = impl;
  }

  fetch():T {
    if (this.configuredFunction === undefined) {
      throw new Error(`Implementation not defined for ${this.name}`)
    }
    return this.configuredFunction;
  }

}

class RegisteredListenerFunctionConfigure<T> extends FunctionConfigure<T> {
  listener: () => void;
  constructor(name: string) {
    super(name);
  }

  configure(impl: T) {
    this.configuredFunction = impl;
    if (this.listener !== undefined) {
      this.listener();
    }
  }

  registerConfigured(listener: () => void) {
    this.listener = listener;
  }
} 

let configuredFunctions = new Set();
let possibleFunctions = new Set();

export function checkConfiguration() {
  const missingFunctions: any[] = [];
  possibleFunctions.forEach((item) => {
    if (!configuredFunctions.has(item)) {
      missingFunctions.push(item);
    }
  });
  if (missingFunctions.length > 0) {
    throw new Error(`Missing Configuration ${missingFunctions}`);
  }
}

export default function<T>(name: string): FunctionConfigure<T> {
  possibleFunctions.add(name);
  const configuration = new RegisteredListenerFunctionConfigure<T>(name); 
  configuration.registerConfigured(() => {
    configuredFunctions.add(name);
  });
  return configuration;
}
