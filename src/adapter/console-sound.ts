import {Sound} from "@dance/play-sound";
import { PlaySound } from "@dance/play-sound";

const playSounds: PlaySound = (sound: Sound) => {
    console.log(`Played: ${sound}`);
};

export default playSounds;
