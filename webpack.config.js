const path = require("path");
const nodeExternals = require("webpack-node-externals");
const { findPaths, runnerExists } = require("./webpack-utils");

const alias = findPaths();

const testing =
    process.env.PRODUCTION === undefined || process.env.PRODUCTION === "false";

const { NODE_ENV = testing ? "development" : "production" } = process.env;


const entry = {
    runner: "./src/app/runner"
};

module.exports = {
    entry: entry,
    mode: NODE_ENV,
    target: "node",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].js",
        devtoolModuleFilenameTemplate: "[absolute-resource-path]",
        devtoolFallbackModuleFilenameTemplate:
            "[absolute-resource-path]?[hash]",
    },
    devtool: "#inline-cheap-module-source-map",
    resolve: {
        extensions: [".ts", ".js"],
        alias: alias,
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig.json",
                },
            },
        ],
    },
};
